import Pages.CreateAccountPage;
import Pages.LandingPage;
import Utils.UserData;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.annotations.*;

import java.io.File;
import java.nio.file.FileSystems;
import java.util.concurrent.TimeUnit;

public class BaseTest {
    WebDriver driver;
    final String separator = File.separator;
    final String chromePath = System.getProperty("user.dir") + separator + "src" + separator + "main" + separator + "resources" + separator + "chromedriver.exe";
    LandingPage landingPage;
    CreateAccountPage createAccountPage;
    UserData user;


    @BeforeSuite
    public void before() {
        System.setProperty("webdriver.chrome.driver", chromePath);
    }

    @BeforeTest
    public void setLandingPage(){
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10L, TimeUnit.SECONDS);
        landingPage = new LandingPage(driver);
        createAccountPage = new CreateAccountPage(driver);
        user = new UserData();
        landingPage.load();
    }

    @DataProvider(name = "Authentication")
    public static Object[][] credentials() {
        return new Object[][] {{ "ala@domain.com", "admin1" }};

    }

    @AfterTest
    public void clean(){
        driver.quit();
    }

}
