import org.testng.annotations.Test;

public class CreateAccountTest extends BaseTest{

    @Test
    public void shouldCreateNewAccount(){
        landingPage
                .clickLoginButton()
                .typeEmailRegistration(user)
                .clickCreateAccountButton()
                .selectTitle(user)
                .typeFirstName(user)
                .typeLastName(user)
                .typePassword(user)
                .typeAddress(user)
                .typeCity(user)
                .typePostalCode(user)
                .selectState(user)
                .typeMobilePhoneNumber(user)
                .clickRegisterButton()
                .assertUserIsOnMyAccountPage();

    }

}
