import org.testng.annotations.Test;

public class AddToCartTest extends BaseTest {

    @Test
    public void unregisteredUserCanAddItemToCartFromLandingPage(){
        landingPage
                .clickAddToCartButton()
                .switchToOverlay()
                .assertHeader2Is("Product successfully added to your shopping cart");
    }
}
