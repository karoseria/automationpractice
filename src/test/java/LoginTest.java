import org.testng.annotations.Test;

public class LoginTest extends BaseTest {

    @Test (dataProvider = "Authentication")
    public void existingUserShouldLogIn( String email, String password){
       landingPage
               .clickLoginButton()
               .typeEmail(email)
               .typePassword(password)
               .clickSingInButton()
               .assertUserIsOnMyAccountPage();
    }
}
