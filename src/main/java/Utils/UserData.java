package Utils;

import static Utils.Helper.*;

public class UserData {

    private static String title;
    private static String email;
    private static String firstName;
    private static String lastName;
    private static String password;
    private static String address;
    private static String city;
    private static String postalCode;
    private static Integer stateNumber;
    private static String mobileNumber;


    public UserData() {
        title = getRandomTitle();
        email = getRandomEmail();
        firstName = getRandomFirstName();
        lastName = getRandomLastName();
        password = getRandomPassword();
        address = getRandomAddress();
        city = geRandomCity();
        postalCode = getRandomPostalCode();
        stateNumber = getRandomStateNumber();
        mobileNumber = getRandomMobileNumber();
    }

    public static String getTitle() {
        return title;
    }

    public String getLastName() {
        return lastName;
    }

    public String getAddress() {
        return address;
    }

    public String getEmail() {
        return email;
    }

    public String getFirstName() {
        return firstName;
    }

    public Integer getStateNumber(){
        return stateNumber;
    }

    public String getPassword() {
        return password;
    }

    public String getCity() {
        return city;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public static void setEmail(String email) {
        UserData.email = email;
    }

    public static void setPassword(String password) {
        UserData.password = password;
    }
}
