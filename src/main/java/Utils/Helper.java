package Utils;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.WebElement;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Random;

import static org.apache.commons.lang3.RandomStringUtils.*;

public class Helper {

    public static void typeText(WebElement element, String text){
        element.clear();
        element.sendKeys(text);
    }

    public static Date generateRandomBirthDate() {
        GregorianCalendar randomBirthDay = new GregorianCalendar();
        randomBirthDay.set(randomBirthDay.YEAR, randBetween(1900, 2018));
        randomBirthDay.set(randomBirthDay.DAY_OF_YEAR, randBetween(1, randomBirthDay.getActualMaximum(randomBirthDay.DAY_OF_YEAR)));
        return randomBirthDay.getTime();
    }

    public static Integer randBetween(int start, int end) {
        return start + (int) Math.round(Math.random() * (end - start));
    }

    public static String getRandomTitle() {
        return new Random().nextBoolean() == true ? "Mr.": "Mrs.";
    }

    public static String getRandomFirstName() {
        return "FN" + randomAlphabetic(4);
    }

    public static String getRandomLastName() {
        return "LN" + randomAlphabetic(6);
    }

    public static String getRandomEmail() {
        return "email" + randomAlphanumeric(6) + "@testdom.xz";
    }

    public static String getRandomCompany() {
        return "Company_" + randomAlphanumeric(5);
    }

    public static Boolean getRandomNewsletterPreference() {
        return new Random().nextBoolean();
    }

    public static String getRandomPassword() {
        return randomAlphanumeric(randBetween(5,30));
    }

    public static String getRandomAddress(){
        return "Address" + randomAlphanumeric(randBetween(0,30));
    }

    public static String geRandomCity(){
        return "City" + randomAlphanumeric(randBetween(0,15));
    }

    public static String getRandomPostalCode(){
       String  postalCode = "";
        for(int i=0 ; i<5; i++){
            Integer digit= new Random().nextInt(10);
            postalCode = postalCode + digit.toString();
        }
        return  postalCode;
    }

    public static int getRandomStateNumber(){
        return randBetween(1,50);
    }

    public static String getRandomMobileNumber(){
        return randomNumeric(14);
    }


}
