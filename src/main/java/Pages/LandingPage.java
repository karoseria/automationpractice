package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class LandingPage extends BasePage {

    public LandingPage(WebDriver driver){
        super(driver);
    }

    @FindBy(className = "login")
    WebElement loginButton;
    @FindBy(css = "[title='Add to cart']")
    WebElement addToCartButton;
    @FindBy(css = "#layer_cart h2")
    WebElement layersHeader;

    public void load() {
        driver.get("http://automationpractice.com/index.php");
    }

    public AuthenticationPage clickLoginButton(){
        loginButton.click();
        return new AuthenticationPage(driver);
    }

    public LandingPage clickAddToCartButton(){
        addToCartButton.click();
        return this;
    }

    public LandingPage switchToOverlay(){
        driver.switchTo().window(driver.getWindowHandles().iterator().next());
        return this;
    }

    public void assertHeader2Is(String expectedText){
        waitForPresenceOfElementLocatedByCss(driver,".icon-ok");
        Assert.assertEquals(layersHeader.getText(), expectedText, "Header2 doesn't match expected text.");
    }
}
