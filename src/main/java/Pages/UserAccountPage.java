package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import static org.testng.Assert.assertEquals;

public class UserAccountPage extends BasePage{
    WebDriver driver;
    final String headerText = "MY ACCOUNT";

    public UserAccountPage(WebDriver driver){
        super(driver);
    }

    @FindBy (css = "h1")
    WebElement header;

    public void assertUserIsOnMyAccountPage(){
        assertEquals(header.getText(), headerText, "Header doesn't match expected text");
    }
}
