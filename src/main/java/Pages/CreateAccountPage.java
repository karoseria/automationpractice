package Pages;

import Utils.UserData;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import static Utils.Helper.typeText;

public class CreateAccountPage extends BasePage {

    public CreateAccountPage(WebDriver driver){
        super(driver);
    }

    @FindBy (id = "id_gender1")
    WebElement titleMrRadiobutton;
    @FindBy (id = "id_gender2")
    WebElement titleMrsRadiobutton;
    @FindBy(id = "customer_firstname")
    WebElement firstNameInput;
    @FindBy(id = "customer_lastname")
    WebElement lastNameInput;
    @FindBy(id = "email")
    WebElement emailInput;
    @FindBy(id = "passwd")
    WebElement passwordInput;
    @FindBy(id = "days")
    WebElement dayOfBirthInput;
    @FindBy(id = "months")
    WebElement monthOfBirthInput;
    @FindBy(id = "years")
    WebElement yearOfBirthsInput;
    @FindBy(id = "newsletter")
    WebElement newsletterCheckbox;
    @FindBy(id = "optin")
    WebElement partnersNewsletterCheckbox;
    @FindBy(id = "firstname")
    WebElement firstNameAddressInput;
    @FindBy(id = "lastname")
    WebElement lastNameAddressInput;
    @FindBy(id = "company")
    WebElement companyAddressInput;
    @FindBy(id = "address1")
    WebElement addressInput;
    @FindBy(id = "city")
    WebElement cityAddressInput;
    @FindBy(id = "id_state")
    WebElement stateAddtressInput;
    @FindBy(id = "postcode")
    WebElement postcodeAddressInput;
    @FindBy(id = "other")
    WebElement additionalInfoInput;
    @FindBy(id = "phone")
    WebElement homePhoneInput;
    @FindBy(id = "phone_mobile")
    WebElement mobileProneInput;
    @FindBy(id = "alias")
    WebElement aliasInput;
    @FindBy(id = "submitAccount")
    WebElement registerButton;

    public CreateAccountPage selectTitle(UserData user){
        if (UserData.getTitle().equals("Mr.")){
            titleMrRadiobutton.click();
        }
        else{
            titleMrsRadiobutton.click();
        }
        return this;
    }

    public CreateAccountPage typeFirstName(UserData user){
        typeText(firstNameInput, user.getFirstName());
        return this;
    }

    public CreateAccountPage typeLastName(UserData user){
        typeText(lastNameInput, user.getLastName());
        return this;
    }

    public CreateAccountPage typePassword(UserData user){
        typeText(passwordInput, user.getPassword());
        return this;
    }

    public CreateAccountPage typeDateOfBirth(UserData user){
//        typeText(dayOfBirthInput, user.getDayOfBirth);
//        typeText(monthOfBirthInput, user.getMonthOfBirth());
//        typeText(yearOfBirthsInput, user.getYearOfBirth());
        return this;
    }

    public CreateAccountPage typeFirstNameAddress(UserData user){
//        typeText(firstNameAddressInput, user.getFirstNameForAddress());
        return this;
    }

    public CreateAccountPage typeLastNameAddress(UserData user){
//        typeText(lastNameAddressInput, user.getLastNameForAddress());
        return this;
    }

    public CreateAccountPage typeAddress(UserData user){
        typeText(addressInput, user.getAddress());
        return this;
    }

    public CreateAccountPage typeCity(UserData user){
        typeText(cityAddressInput, user.getCity());
        return this;
    }

    public CreateAccountPage typePostalCode(UserData user){
        typeText(postcodeAddressInput, user.getPostalCode());
        return this;
    }

    public CreateAccountPage selectState(UserData user){
        Select stateSelect = new Select(stateAddtressInput);
        stateSelect.selectByValue(user.getStateNumber().toString());
        return this;
    }

    public CreateAccountPage typeMobilePhoneNumber(UserData user){
        typeText(mobileProneInput, user.getMobileNumber());
    return this;
    }

    public UserAccountPage clickRegisterButton(){
        registerButton.click();
        return new UserAccountPage(driver);
    }
}
