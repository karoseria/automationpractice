package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BasePage extends PageFactory {
    protected WebDriver driver;

    public BasePage(WebDriver driver){
        this.driver = driver;
        initElements(driver, this);
    }

    protected void waitForPresenceOfElementLocatedByCss(WebDriver driver, String locator){
        new WebDriverWait(driver, 5)
                .until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(locator)));
    }
}
