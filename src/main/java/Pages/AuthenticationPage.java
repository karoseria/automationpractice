package Pages;

import Utils.UserData;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static Utils.Helper.typeText;

public class AuthenticationPage extends BasePage {

    public AuthenticationPage(WebDriver driver){
        super(driver);
    }

    @FindBy (id = "email_create")
    WebElement emailRegistrationInput;
    @FindBy (id = "SubmitCreate")
    WebElement createAccountButton;
    @FindBy (id = "email")
    WebElement emailLoginInput;
    @FindBy (id = "passwd")
    WebElement passwordLoginInput;
    @FindBy (id = "SubmitLogin")
    WebElement signInButton;

    public AuthenticationPage typeEmailRegistration (UserData user){
        typeText(emailRegistrationInput, user.getEmail());
        return this;
    }

    public CreateAccountPage clickCreateAccountButton(){
        createAccountButton.click();
        return new CreateAccountPage(driver);
    }
    public AuthenticationPage typeEmail(String email){
        typeText(emailLoginInput, email);
        return this;
    }

    public AuthenticationPage typePassword(String pass){
        typeText(passwordLoginInput, pass);
        return this;
    }

    public UserAccountPage clickSingInButton(){
        signInButton.click();
        return new UserAccountPage(driver);
    }

}
